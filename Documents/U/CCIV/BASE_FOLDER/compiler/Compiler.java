import compiler.scanner.Scanner;
import compiler.parser.CC4Parser;
import compiler.ast.Ast;
import compiler.semantic.Semantic;
import compiler.irt.Irt;
import compiler.codegen.Codegen;
import compiler.opt.Algebraic;
import compiler.opt.ConstantFolding;
import compiler.lib.Debug;
import compiler.lib.ErrorHandler;
import java.io.*;

public class Compiler
{
	public static void main(String[] args)
	{

		try{

			PrintStream outStream = null;
				
			String prevCommand = "";

			String outputFilename = "";
			String inputFilename = "";
			String target = "";
			String debug= "";
			String opt= "";


			for(int i=0; i<args.length; i++ )
			{

				
				switch (args[i].toLowerCase()){
					case "-help":
					case "-h":
						printHelp(prevCommand);
						break;
					case "-o":
					case "-output":
						i++;
						if(i>=args.length || args[i].startsWith("-"))
						{
							System.out.println("Error: Esperando nombre del archivo de salida");
							return;
						}
						outputFilename = args[i];
						outStream = new PrintStream(new FileOutputStream(outputFilename));
						System.setOut(outStream);

						break;
					case "-target":
						i++;
						if(i>=args.length || args[i].startsWith("-"))
						{
							String mensaje = "Error: Esperando nombre de stage";
							System.out.println(mensaje);
							return;
						}

						switch (args[i].toLowerCase())
						{
							case "scan":
							case "parse":
							case "ast":
							case "semantic":
							case "irt":
							case "codegen":
								target = args[i];
								break;
							default:
								String mensaje = "Error: Stage desconocido. Use -h para ver ayuda del comando \"stage\".";
								System.out.println(mensaje);
								return;		
						}
						break;

					case "-opt":
						i++;
						if(i>=args.length || args[i].startsWith("-"))
						{
							String mensaje = "Error: Esperando tipo de optimizacion";
							System.out.println(mensaje);
							return;
						}

						switch (args[i].toLowerCase())
						{
							case "algebraic":
							case "constant":
								opt = args[i];
								break;
							default:
								String mensaje = "Error: optimizacion desconosida. Use -h para ver ayuda del comando \"opt\".";
								System.out.println(mensaje);
								return;		
						}
						break;

					case "-debug":
						i++;
						if(i>=args.length || args[i].startsWith("-"))
						{
							String mensaje = "Error: Esperando nombre de stage";
							System.out.println(mensaje);
							return;
						}
						
						debug = args[i];
						
						break;

					default:
						if(i==args.length-1 && !args[i].startsWith("-"))
						{
							inputFilename = args[i];
						}else
						{
							System.out.println("Comando desconocido \""+ args[i] +"\". Use -help para ver la ayuda");	
							return;
						}
						break;
				}
			}

			
			Compilar(inputFilename, outputFilename, target, debug, opt );
			
		}catch(IOException ex)
		{
			System.out.println(ex);
		}
	}

	private static void printHelp(String command)
	{	
		if(command != "")
		{
			System.out.println("Aqui va la ayuda del comando "+command);

		}else
		{
			System.out.println("-o <outname>		Escribir el output a <outname>.");
			System.out.println("");	
			System.out.println("-target <stage>		Donde <stage> es uno de: scan, parse, ast, semantic, irt, codegen; la compilación debe proceder hasta la etapa indicada. ");
			System.out.println("");									
			System.out.println("			Por ejemplo, si <stage> es scan, una instancia de scan debe ser creada imprimiendo en el archivo de salida \"stage: scanning\". ");
			System.out.println("");		
			System.out.println("			Si es parse una instancia de parser debe ser creada a partir de la instancia de scanner imprimiendo \"stage: parsing\", ");
			System.out.println("			además del mensaje de scanner y así sucesivamente. ");
			System.out.println("");		
			System.out.println("-opt <opt_stage>	<opt_stage> es uno de: constant, algebraic; la compilación debe hacer solo la optimización que se le pida,");
			System.out.println("			y debe imprimir como en -target \"optimizing: constant folding\" o \"optimizing: algebraic simplification\".");
			System.out.println("");
			System.out.println("-debug <stage>		Imprimir información de debugging. Debe haber un mensaje por cada etapa listada en <stage> de la forma \"Debugging <stage>\". ");
			System.out.println("			<stage> tiene las mismas opciones de -target, con la diferencia que se pueden \"debuggear\" varias etapas, ");
			System.out.println("			separandolas con ':' de la forma scan:parse:etc.");
			System.out.println("");		
			System.out.println("-h");
		}
	}

	private static void Compilar(String inputFilename, String outputFilename, String target, String debug, String opt)
	{
			
				if(inputFilename == "")
				{
					String mensaje = "Error: Ningun nombre de archivo de entrada";
					System.out.println(mensaje);
					return;
					
				}


				boolean debugScanner = false;
				boolean debugParser = false;
				boolean debugAst = false;
				boolean debugSemantic = false;
				boolean debugIrt = false;
				boolean debugCodegen = false;
				boolean debugAlgebraic = false;
				boolean debugConstant = false;

				String[] debugStages = debug.toLowerCase().split(":");
				for(int i = 0; i<debugStages.length; i++)
				{
					switch(debugStages[i])
					{
						case("scanner"):
							debugScanner = true;
							break;
						case("parser"):
							debugParser = true;
							break;
						case("ast"):
							debugAst = true;
							break;
						case("semantic"):
							debugSemantic = true;
							break;
						case("irt"):
							debugIrt = true;
							break;
						case("codegen"):
							debugCodegen = true;
							break;
						case("algebraic"):
							debugAlgebraic = true;
							break;							
						case("constant"):
							debugConstant = true;
							break;							
					}
				}


				Scanner scanner = new Scanner(inputFilename, debugScanner);
				if(target=="scanner")
					return;
				CC4Parser parser = new CC4Parser(scanner, debugParser);
				if(target=="parser")
					return;
				Ast ast = new Ast(parser, debugAst);
				if(target=="ast")
					return;
				Semantic semantic = new Semantic(ast, debugSemantic);
				if(target=="semantic")
					return;
				Irt irt = new Irt(semantic, debugIrt);
				if(target=="irt")
					return;
				Codegen codegen = new Codegen(irt, debugCodegen);
				
				System.out.println(opt);
				if(opt.equalsIgnoreCase("algebraic"))
				{
					Algebraic algebraic = new Algebraic(codegen, debugAlgebraic);
				}else if (opt.equalsIgnoreCase("constant"))
				{
					ConstantFolding constant = new ConstantFolding(codegen, debugConstant);
				}

	}

}